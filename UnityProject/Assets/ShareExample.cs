﻿using UnityEngine;

public class ShareExample : MonoBehaviour
{
	[SerializeField] private Texture2D texture;
	[SerializeField] private ShareController sharer;

	public void ShareText()
	{
		sharer.Native_ShareText("Share some text...");
	}

	public void ShareTextAndTexture2D()
	{
		sharer.Native_ShareTextWithTexture("Share text and texture...", texture);
	}
}