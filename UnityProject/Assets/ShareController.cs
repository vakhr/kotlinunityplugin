﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class ShareController : MonoBehaviour
{
    #if UNITY_ANDROID
	private const string SharerClassName = "com.wazzapps.sharing.Sharer";
	private AndroidJavaObject sharer;
    #endif

    #if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void ios_ShareText(string text);

	[DllImport("__Internal")]
	private static extern void ios_ShareTextWithImage(string text, string imagePath);
	#endif

	#if UNITY_ANDROID
	private void Awake()
	{
		sharer = new AndroidJavaObject(SharerClassName);
	}
	#endif

	public void Native_ShareText(string text)
	{
		#if UNITY_ANDROID
		share(text);
		#elif UNITY_IOS
		ios_ShareText(text);
		#endif
	}

	public void Native_ShareTextWithTexture(string text, Texture2D texture)
	{
		var path = SaveTexture(texture);

		#if UNITY_ANDROID
		shareTexture(text, path);
		#elif UNITY_IOS
		ios_ShareTextWithImage(text, path);
		#endif
	}

	#if UNITY_ANDROID
	private void share(string message)
	{
		sharer.Call(nameof(share), message);
	}
	#endif

	#if UNITY_ANDROID
	private void shareTexture(string message, string path, string type = "image/*")
	{
		sharer.Call(nameof(shareTexture), message, path, type);
	}
	#endif

	private string SaveTexture(Texture2D texture)
	{
		var fileName = DateTime.Now
			.ToString(CultureInfo.InvariantCulture)
			.Replace(" ", "_")
			.Replace("/", "_")
			.Replace(":", "_") + ".png";

		var path = $"{Application.persistentDataPath}/{fileName}";
		var bytes = texture.EncodeToPNG();

		File.WriteAllBytes(path, bytes);
		return path;
	}
}