package com.wazzapps.sharing

import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider

import com.unity3d.player.UnityPlayer

import java.io.File

class Sharer {
    fun share(message: String?) {
        val sendIntent = Intent()

        sendIntent.type = "text/type"
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, message)

        UnityPlayer.currentActivity.startActivity(sendIntent)
    }

    fun shareTexture(message: String?, path: String?, type: String?) {
        val sendIntent = Intent()

        val file = File(path)
        if (file.exists()) {
            var uri: Uri = Uri.fromFile(file)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(
                    UnityPlayer.currentActivity,
                    UnityPlayer.currentActivity.packageName + ".provider",
                    file
                )
            }

            sendIntent.putExtra(Intent.EXTRA_STREAM, uri)
        }

        sendIntent.type = type
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, message)

        UnityPlayer.currentActivity.startActivity(sendIntent)
    }
}