import UIKit

@_cdecl("ios_ShareText")
public func ShareText(inputText: UnsafePointer<CChar>?) {
    if (inputText == nil) {
        return
    }

    let text = String(cString: inputText!)
    let shareActivity = UIActivityViewController(activityItems: [text], applicationActivities: nil)

    let rootViewController = UIApplication.shared.keyWindow?.rootViewController
    rootViewController?.present(shareActivity, animated: true, completion: nil)
}

@_cdecl("ios_ShareTextWithImage")
public func ShareTextWithImage(inputText: UnsafePointer<CChar>?, imagePath: UnsafePointer<CChar>?) {
    if inputText == nil || imagePath == nil {
        return
    }

    let text = String(cString: inputText!)
    let path = String(cString: imagePath!)
    let image = UIImage(contentsOfFile: path)
    let shareActivity = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)

    let rootViewController = UIApplication.shared.keyWindow?.rootViewController
    rootViewController?.present(shareActivity, animated: true, completion: nil)
}
